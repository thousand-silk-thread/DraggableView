# 作者声明：
本库基于原项目（https://github.com/elevenetc/DraggableView）开发openharmony项目。
由于openharmony暂时没有找到让Component转PixelMap的方案，所以这里暂时只支持Image设置image_src图片的方式，
并且设置ohos:scale_mode="stretch" 这样可以暂且做到与原库效果完全一致。
待有Component转PixelMap方案将会再更新。

# DraggableView
Draggable image with rotation and skew/scale effects.

## SkewView
![animation](art/demo1.gif)

## RotateView
![animation](art/demo2.gif)

### Usage

```
dependencies{
   implementation 'io.openharmony.tpc.thirdlib:DraggableView:1.0.0'
}
```

1. Implement [`DragController.IDragViewGroup`](library/src/main/java/su/levenetc/ohos/draggableview/DragController.java)
2. Create instance of [`DragController`](library/src/main/java/su/levenetc/ohos/draggableview/DragController.java)
3. Override `onTouchEvent` of your `ComponentContainer` and call `DragController#onTouchEvent`:
```Java
@Override public boolean onTouchEvent(MotionEvent event, Component component) {
	return dragController.onTouchEvent(event, component);
}
```
See full sample at [`SampleGridContainer`](sample/src/main/java/su/levenetc/ohos/sample/SampleGridContainer.java)
### Animation adjustments
[`SkewView`](library/src/main/java/su/levenetc/ohos/draggableview/SkewView.java) and [`RotateView`](library/src/main/java/su/levenetc/ohos/draggableview/RotateView.java) containe multipliers which change rotation, skew and scale values.

### Licence
http://www.apache.org/licenses/LICENSE-2.0
